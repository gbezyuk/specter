from .default import *

INSTALLED_APPS += [
    'django_filters',
    'rest_framework',
    'corsheaders',
    'filebrowser',
    'debug_toolbar',
    'django_extensions',

    'common',
    'core',
    'specter',
    'rest_api',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'corsheaders.middleware.CorsMiddleware',
] + MIDDLEWARE + [
    'common.current_user.middleware.CurrentUserMiddleware',
]

USE_I18N = True
USE_L10N = True
TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-ru'

CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend']
}

INTERNAL_IPS = [
    '127.0.0.1',
]

ALLOWED_HOSTS = [
    '127.0.0.1',
]

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': "%s.true" % __name__,
}

def true(request):
    return True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'specter',
        'USER': 'specter',
        'PASSWORD': 'specter',
    }
}
