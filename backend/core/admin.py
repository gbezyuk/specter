from django.contrib.admin import site as default_admin_site
# from users.models.register_model_admins import register_model_admins as register_users
from specter.models.register_model_admins import register_model_admins as register_specter

# register_users(default_admin_site)
register_specter(default_admin_site)
