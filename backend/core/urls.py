from django.contrib import admin
from django.urls import path, include
from filebrowser.sites import site as filebrowser_site

urlpatterns = [
    path('admin/filebrowser/', filebrowser_site.urls),
    path('admin/', admin.site.urls),
    path('api/', include('rest_api.urls')),
]


from django.conf import settings

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root = settings.MEDIA_ROOT,
        show_indexes = True
    )

    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
