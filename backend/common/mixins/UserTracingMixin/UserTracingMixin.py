from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from ...current_user.storage import get_current_user


class UserTracingMixin (models.Model):
    class Meta:
        abstract = True

    created_by = models.ForeignKey(
        verbose_name = _('created by'),
        to = settings.AUTH_USER_MODEL,
        null = True,
        blank = True,
        editable = False,
        on_delete = models.PROTECT,
        related_name = '+',
    )
    updated_by = models.ForeignKey(
        verbose_name = _('updated by'),
        to = settings.AUTH_USER_MODEL,
        null = True,
        blank = True,
        editable = False,
        on_delete = models.PROTECT,
        related_name = '+',
    )

    def save(self, *args, **kwargs):
        user = get_current_user()
        if user and user.is_anonymous:
            user = None
        if self._state.adding:
            self.created_by = user
        self.updated_by = user
        super().save(*args, **kwargs)
