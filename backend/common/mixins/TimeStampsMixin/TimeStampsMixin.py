from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimeStampsMixin (models.Model):
    class Meta:
        abstract = True

    created_on = models.DateTimeField(
        verbose_name = _('created on'),
        auto_now_add = True,
    )

    updated_on = models.DateTimeField(
        verbose_name = _('updated on'),
        auto_now = True,
    )
