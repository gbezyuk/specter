from django.db import models
from django.utils.translation import ugettext_lazy as _
import uuid
from ...mixins import TimeStampsMixin, UserTracingMixin


class AbstractBaseModel (
    # models.Model,
    TimeStampsMixin,
    UserTracingMixin
):

    class Meta:
        abstract = True


    uuid = models.UUIDField(
        verbose_name = _('UUID'),
        primary_key = True,
        default = uuid.uuid4,
        editable = False
    )
