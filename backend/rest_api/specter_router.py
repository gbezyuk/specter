from rest_framework.routers import DefaultRouter
from specter.models.Specter.RFW.SpecterViewSet import SpecterViewSet


router = DefaultRouter()
router.register('specters', SpecterViewSet)
