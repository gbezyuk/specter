from django.urls import path, include
from .whoami.view import MeAPIView
from .specter_router import router as specter_router


urlpatterns = [
    path('jwt/', include('rest_api.jwt_auth.urls')),
    path('me/', MeAPIView.as_view()),
    path('specter/', include(specter_router.urls)),
]
