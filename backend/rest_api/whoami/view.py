from django.contrib.auth import get_user_model
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from .serialzier import MeSerializer

UserModel = get_user_model()


class MeAPIView(RetrieveAPIView):

    queryset = UserModel.objects.all()
    serializer_class = MeSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user
