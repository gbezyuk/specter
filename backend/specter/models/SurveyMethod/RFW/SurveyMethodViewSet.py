from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from ..SurveyMethod import SurveyMethod
from .SurveyMethodSerializer import SurveyMethodSerializer


class SurveyMethodPagination (PageNumberPagination):
    page_size = 100
    max_page_size = 10000


class SurveyMethodViewSet (ModelViewSet):
    queryset = SurveyMethod.objects.all()
    serializer_class = SurveyMethodSerializer
    pagination_class = SurveyMethodPagination
