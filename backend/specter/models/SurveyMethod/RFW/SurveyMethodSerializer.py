from rest_framework.serializers import ModelSerializer
from ..SurveyMethod import SurveyMethod


class SurveyMethodSerializer (ModelSerializer):
    class Meta:
        model = SurveyMethod
        fields = (
            'uuid',
            'name',
            'created_on',
            'created_by',
            'updated_on',
            'updated_by',
        )
