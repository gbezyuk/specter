from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel


class SurveyMethod (AbstractBaseModel):

    class Meta:
        verbose_name = _('survey method')
        verbose_name_plural = _('survey methods')
        default_related_name = 'survey_methods'

    def __str__ (self):
        return self.name

    name = models.CharField(
        verbose_name = _('name'),
        null = False,
        blank = False,
        unique = True,
        max_length = 1024,
    )
