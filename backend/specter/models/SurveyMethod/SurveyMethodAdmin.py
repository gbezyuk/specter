from django.contrib.admin import ModelAdmin


class SurveyMethodAdmin (ModelAdmin):

    list_display = (
        'uuid',
        'name',
        'created_on',
        'created_by',
        'updated_on',
        'updated_by',
    )

    list_display_links = (
        'uuid',
        'name',
    )

    search_fields = (
        'name',
    )
