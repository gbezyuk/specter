from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel


class Material (AbstractBaseModel):

    class Meta:
        verbose_name = _('material')
        verbose_name_plural = _('materials')
        default_related_name = 'materials'

    def __str__ (self):
        return self.name

    name = models.CharField(
        verbose_name = _('name'),
        null = False,
        blank = False,
        unique = True,
        max_length = 1024,
    )
