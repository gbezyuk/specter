from rest_framework.serializers import ModelSerializer
from ..Material import Material


class MaterialSerializer (ModelSerializer):
    class Meta:
        model = Material
        fields = (
            'uuid',
            'name',
            'created_on',
            'created_by',
            'updated_on',
            'updated_by',
        )
