from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from ..Material import Material
from .MaterialSerializer import MaterialSerializer


class MaterialPagination (PageNumberPagination):
    page_size = 100
    max_page_size = 10000


class MaterialViewSet (ModelViewSet):
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer
    pagination_class = MaterialPagination
