from django.contrib.admin import ModelAdmin


class MaterialAdmin (ModelAdmin):

    list_display = (
        'name',
        'uuid',
        'created_on',
        'created_by',
        'updated_on',
        'updated_by',
    )

    list_display_links = (
        'uuid',
        'name',
    )

    search_fields = (
        'name',
    )
