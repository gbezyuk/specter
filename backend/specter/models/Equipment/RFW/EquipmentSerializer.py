from rest_framework.serializers import ModelSerializer
from ..Equipment import Equipment


class EquipmentSerializer (ModelSerializer):
    class Meta:
        model = Equipment
        fields = (
            'uuid',
            'name',
            'created_on',
            'created_by',
            'updated_on',
            'updated_by',
        )
