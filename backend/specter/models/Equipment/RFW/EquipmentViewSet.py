from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from ..Equipment import Equipment
from .EquipmentSerializer import EquipmentSerializer


class EquipmentPagination (PageNumberPagination):
    page_size = 100
    max_page_size = 10000


class EquipmentViewSet (ModelViewSet):
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializer
    pagination_class = EquipmentPagination
