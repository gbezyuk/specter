from .Equipment import Equipment
from .Equipment.EquipmentAdmin import EquipmentAdmin

from .Specter import Specter
from .Specter.SpecterAdmin import SpecterAdmin

from .SurveyMethod import SurveyMethod
from .SurveyMethod.SurveyMethodAdmin import SurveyMethodAdmin

from .Material import Material
from .Material.MaterialAdmin import MaterialAdmin

from .OpticalProperty import OpticalProperty
from .OpticalProperty.OpticalPropertyAdmin import OpticalPropertyAdmin

from .SpecterPoint import SpecterPoint
from .SpecterPoint.SpecterPointAdmin import SpecterPointAdmin


def register_model_admins (admin_site):
    admin_site.register(Equipment, EquipmentAdmin)
    admin_site.register(Specter, SpecterAdmin)
    admin_site.register(SurveyMethod, SurveyMethodAdmin)
    admin_site.register(Material, MaterialAdmin)
    admin_site.register(OpticalProperty, OpticalPropertyAdmin)
    admin_site.register(SpecterPoint, SpecterPointAdmin)
