from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from ..OpticalProperty import OpticalProperty
from .OpticalPropertySerializer import OpticalPropertySerializer


class OpticalPropertyPagination (PageNumberPagination):
    page_size = 100
    max_page_size = 10000


class OpticalPropertyViewSet (ModelViewSet):
    queryset = OpticalProperty.objects.all()
    serializer_class = OpticalPropertySerializer
    pagination_class = OpticalPropertyPagination
