from rest_framework.serializers import ModelSerializer
from ..OpticalProperty import OpticalProperty


class OpticalPropertySerializer (ModelSerializer):
    class Meta:
        model = OpticalProperty
        fields = (
            'uuid',
            'name',
            'created_on',
            'created_by',
            'updated_on',
            'updated_by',
        )
