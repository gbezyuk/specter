from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from ..SpecterPoint import SpecterPoint
from .SpecterPointSerializer import SpecterPointSerializer


class SpecterPointPagination (PageNumberPagination):
    page_size = 100
    max_page_size = 10000


class SpecterPointViewSet (ModelViewSet):
    queryset = SpecterPoint.objects.all()
    serializer_class = SpecterPointSerializer
    pagination_class = SpecterPointPagination
