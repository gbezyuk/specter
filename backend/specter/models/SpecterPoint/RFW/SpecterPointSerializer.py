from rest_framework.serializers import ModelSerializer
from ..SpecterPoint import SpecterPoint


class SpecterPointSerializer (ModelSerializer):
    class Meta:
        model = SpecterPoint
        fields = (
            'uuid',
            'specter',
            'x',
            'y',
            'z',
            'value',
            'created_on',
            'created_by',
            'updated_on',
            'updated_by',
        )
