from django.contrib.admin import TabularInline
from .SpecterPoint import SpecterPoint


class SpecterPointAdminInlineReadonly (TabularInline):
    model = SpecterPoint
    extra = 0
    readonly_fields = (
        'x',
        'y',
        'z',
        'value',
    )
