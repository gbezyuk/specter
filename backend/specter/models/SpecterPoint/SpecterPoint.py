from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel


# class SpecterPoint (AbstractBaseModel):
class SpecterPoint (models.Model):

    class Meta:
        verbose_name = _('specter point')
        verbose_name_plural = _('specter points')
        default_related_name = 'specter_points'
        ordering = ( 'specter', 'x', )
        unique_together = ( ('specter', 'x', 'y', 'z',), )
        indexes = (
            models.Index(fields = ('specter', 'x', 'y', 'z',)),
        )

    def __str__ (self):
        return f"{self.specter}: ({self.x}, {self.y}, {self.z}) : {self.value}"

    specter = models.ForeignKey(
        verbose_name = _('specter'),
        to = 'Specter',
        null = False,
        blank = False,
        on_delete = models.CASCADE,
        db_index = True,
    )

    x = models.FloatField(
        verbose_name = _('x'),
        null = False,
        blank = False,
        default = 0,
        db_index = True,
    )

    y = models.FloatField(
        verbose_name = _('y'),
        null = False,
        blank = False,
        default = 0,
    )

    z = models.FloatField(
        verbose_name = _('z'),
        null = False,
        blank = False,
        default = 0,
    )

    value = models.FloatField(
        verbose_name = _('value'),
        null = False,
        blank = False,
        default = 0,
    )
