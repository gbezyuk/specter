from django.contrib.admin import ModelAdmin


class SpecterPointAdmin (ModelAdmin):

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('specter')

    list_display = (
        'pk',
        'specter',
        'x',
        'y',
        'z',
        'value',
        # 'uuid',
        # 'created_on',
        # 'created_by',
        # 'updated_on',
        # 'updated_by',
    )

    list_display_links = (
        # 'uuid',
        'specter',
    )

    list_filter = (
        'specter',
        'x',
        'y',
        'z',
    )

    search_fields = (
        # 'specter__name',
    )
