from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel
from filebrowser.fields import FileBrowseField


class Specter (AbstractBaseModel):

    class Meta:
        verbose_name = _('specter')
        verbose_name_plural = _('specters')
        default_related_name = 'specters'

    def __str__ (self):
        return self.name or self.uuid

    name = models.CharField(
        verbose_name = _('name'),
        null = True,
        blank = True,
        max_length = 1024,
    )

    comment = models.TextField(
        verbose_name = _('comment'),
        null = True,
        blank = True,
    )

    x_floor = models.FloatField(
        verbose_name = _('x floor'),
        null = False,
        blank = False,
        default = 0,
    )

    x_step = models.FloatField(
        verbose_name = _('x step'),
        null = False,
        blank = False,
        default = 0,
    )

    x_ceil = models.FloatField(
        verbose_name = _('x ceil'),
        null = False,
        blank = False,
        default = 0,
    )

    y_floor = models.FloatField(
        verbose_name = _('y floor'),
        null = False,
        blank = False,
        default = 0,
    )

    y_step = models.FloatField(
        verbose_name = _('y step'),
        null = False,
        blank = False,
        default = 0,
    )

    y_ceil = models.FloatField(
        verbose_name = _('y ceil'),
        null = False,
        blank = False,
        default = 0,
    )

    z_floor = models.FloatField(
        verbose_name = _('z floor'),
        null = False,
        blank = False,
        default = 0,
    )

    z_step = models.FloatField(
        verbose_name = _('z step'),
        null = False,
        blank = False,
        default = 0,
    )

    z_ceil = models.FloatField(
        verbose_name = _('z ceil'),
        null = False,
        blank = False,
        default = 0,
    )

    range_floor = models.FloatField(
        verbose_name = _('range floor'),
        null = True,
        blank = True,
    )

    range_ceil = models.FloatField(
        verbose_name = _('range ceil'),
        null = True,
        blank = True,
    )

    laser_wavelength = models.FloatField(
        verbose_name = _('laser wavelength'),
        null = True,
        blank = True,
    )

    snr = models.FloatField(
        verbose_name = _('SNR'),
        null = True,
        blank = True,
    )

    equipment = models.ForeignKey(
        verbose_name = _('equipment'),
        to = 'Equipment',
        null = True,
        blank = True,
        on_delete = models.PROTECT,
    )

    method = models.ForeignKey(
        verbose_name = _('survey method'),
        to = 'SurveyMethod',
        null = True,
        blank = True,
        on_delete = models.PROTECT,
    )

    material = models.ForeignKey(
        verbose_name = _('material'),
        to = 'Material',
        null = True,
        blank = True,
        on_delete = models.PROTECT,
    )

    optical_property = models.ForeignKey(
        verbose_name = _('optical property'),
        to = 'OpticalProperty',
        null = True,
        blank = True,
        on_delete = models.PROTECT,
    )

    visualization = FileBrowseField(
        verbose_name = _('visualization'),
        null = True,
        blank = True,
        max_length = 1000,
        directory = "visualizations/",
        extensions = ( ".jpg", ".jpeg", ".png" ),
    )

    binary_data = FileBrowseField(
        verbose_name = _('binary data'),
        null = True,
        blank = True,
        max_length = 1000,
        directory = "binary_data/",
        extensions = ( ".dat", "", ),
    )

    csv_data = FileBrowseField(
        verbose_name = _('csv data'),
        null = True,
        blank = True,
        max_length = 1000,
        directory = "csv_data/",
        extensions = ( ".csv", ),
    )


    @classmethod
    def createFromCsv (cls, path):
        import csv
        from numpy import arange
        from datetime import datetime
        from .. import SpecterPoint

        with open(path) as csvfile:
            reader = csv.reader(csvfile)

            now = str(datetime.now())

            instance = cls()
            instance.csv_data = path.replace('./media/', '')
            instance.name = path
            instance.comment = 'imported from csv on ' + now

            instance.x_floor = 0.0
            instance.y_floor = 0.0
            instance.x_ceil = float(reader.__next__()[1])
            instance.x_ceil = 3
            instance.y_ceil = float(reader.__next__()[1])
            instance.y_ceil = 3
            instance.x_step = float(reader.__next__()[1])
            instance.y_step = float(reader.__next__()[1])
            instance.z_floor = 0.0
            instance.z_step = 1.0
            instance.z_ceil = float(reader.__next__()[1])
            instance.z_ceil = 2

            instance.save()

            bulk = []

            for x in arange(instance.x_floor, instance.x_ceil, instance.x_step):
                for y in arange(instance.y_floor, instance.y_ceil, instance.y_step):
                    row = reader.__next__()
                    # SpecterPoint.objects.bulk_create([
                    #     SpecterPoint(specter = instance, x = x, y = y, z = z, value = float(row[int(z / instance.z_step)]))
                    #     for z in arange(instance.z_floor, instance.z_ceil, instance.z_step)
                    # ])
                    for z in arange(instance.z_floor, instance.z_ceil, instance.z_step):
                        # print((x, y, z), end='; ')
                        bulk.append(SpecterPoint(specter = instance, x = x, y = y, z = z, value = float(row[int(z / instance.z_step)])))
                    # print('x, y', x, y)
                print('x', x)

            print('running bulk create')
            SpecterPoint.objects.bulk_create(bulk)
            print('bulk create done')


# """
# Оборудование: teraview или др
# Метод : пропускание / отражение / npvo
# Материал (выбор или создание нового пункта): Био/оптика/краска/картины/другие
# ТГц оптические свойства (выбор) : коэффициент поглощение / …
# Формат файла : dat, csv
# превью для массива ху - график
# Диапазон : ТГц , ...
# Длина волны лазера : ...
# //Дополнительно : Оптическое разрешение и CNR - сигнал/шум . //
# """
