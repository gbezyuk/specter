from rest_framework.serializers import ModelSerializer
from ..Specter import Specter


class SpecterSerializer (ModelSerializer):
    class Meta:
        model = Specter
        fields = (
            'name',
            'comment',
            'binary_data',
            'csv_data',
            'visualization',
            'x_floor',
            'x_step',
            'x_ceil',
            'y_floor',
            'y_step',
            'y_ceil',
            'z_floor',
            'z_step',
            'z_ceil',
            'range_floor',
            'range_ceil',
            'laser_wavelength',
            'snr',
            'method',
            'material',
            'optical_property',
            'equipment',
            'uuid',
            'created_on',
            'created_by',
            'updated_on',
            'updated_by',
        )
