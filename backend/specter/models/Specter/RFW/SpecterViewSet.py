from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from ..Specter import Specter
from .SpecterSerializer import SpecterSerializer


class SpecterPagination (PageNumberPagination):
    page_size = 100
    max_page_size = 10000


class SpecterViewSet (ModelViewSet):
    queryset = Specter.objects.all()
    serializer_class = SpecterSerializer
    pagination_class = SpecterPagination
