from django.contrib.admin import ModelAdmin
# from ..SpecterPoint.SpecterPointAdminInline import SpecterPointAdminInline
from ..SpecterPoint.SpecterPointAdminInlineReadonly import SpecterPointAdminInlineReadonly
from filebrowser.settings import ADMIN_THUMBNAIL
from django.utils.safestring import mark_safe

class SpecterAdmin (ModelAdmin):

    date_hierarchy = 'created_on'

    list_display = (
        'thumbnail',
        'name',
        'comment',
        'x_floor',
        'x_step',
        'x_ceil',
        'y_floor',
        'y_step',
        'y_ceil',
        'z_floor',
        'z_step',
        'z_ceil',
        'range_floor',
        'range_ceil',
        'laser_wavelength',
        'snr',
        'method',
        'material',
        'optical_property',
        'equipment',
        'binary_data',
        'csv_data',
        'uuid',
        'created_on',
        'created_by',
        'updated_on',
        'updated_by',
    )

    list_display_links = (
        'uuid',
        'name',
        'thumbnail',
        'comment',
    )

    list_filter = (
        'x_floor',
        'x_step',
        'x_ceil',
        'y_floor',
        'y_step',
        'y_ceil',
        'z_floor',
        'z_step',
        'z_ceil',
        'method',
        'material',
        'optical_property',
        'equipment',
        'created_by',
        'updated_by',
    )

    search_fields = (
        'name',
    )

    inlines = (
        # SpecterPointAdminInline,
        SpecterPointAdminInlineReadonly,
    )

    def thumbnail (self, obj):
        if obj.visualization and obj.visualization.filetype == "Image":
            return mark_safe('<img src="%s" />' % obj.visualization.version_generate(ADMIN_THUMBNAIL).url)
        else:
            return ""

