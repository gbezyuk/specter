from django.core.management.base import BaseCommand
from ...models import Specter


class Command(BaseCommand):
    help = 'Import specter from a csv file'

    def add_arguments (self, parser):
        parser.add_argument(
            'path',
            type = str,
            help = 'path to the `specter-name.csv` file',
            default = './specter.csv',
        )

    def handle (self, *args, **kwargs):
        path = kwargs['path']
        Specter.createFromCsv(path)
