from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SpecterAppConfig(AppConfig):
    name = 'specter'
    verbose_name = _('Specter')
